#include <Arduino.h>
#include "SSD1306Wire.h"

#define SDAPin 22
#define SCLPin 21
#define PUSHPin 5
#define MT6701Address 0x06 // MT6701 i2C Address
#define MT6701MSB 0x03     // i2C register for MSB
#define MT6701LSB 0x04     // i2C register for LSB
bool push = false;
static const float ALPHA = 0.4;

SSD1306Wire display(0x3c, SDA, SCL); // ADDRESS, SDA, SCL

float MT6701_Read()
{
  // read MT6701 i2C angle registers 3 & 4
  // and return with a float angle of the magnetic field of the dipolar magnet
  Wire.beginTransmission(MT6701Address); // setup for talking to the MT6701 IC
  Wire.write(3);                         // set register for read 1st register (3)
  Wire.endTransmission(false);           // Send the restart condition
  uint8_t bytesReceived = Wire.requestFrom(MT6701Address, 2);
  uint8_t temp[bytesReceived];
  Serial.printf("requestFrom: %u\n", bytesReceived);
  if ((bool)bytesReceived)
  { // If received more than zero bytes
    Wire.readBytes(temp, bytesReceived);
  }
  uint16_t spi_16 = (temp[0] << 8) | temp[1]; // MSB, LSB
  uint16_t angle_i2c = spi_16 >> 2;
  Serial.println(angle_i2c);
  float new_angle = (float)angle_i2c * 360 / 16384;
  Serial.println(new_angle);
  return new_angle;
  /*
  float new_angle = (float)angle_i2c * 2 * PI / 16384;
  float new_x = cosf(new_angle);
  float new_y = sinf(new_angle);
  float x_ = new_x * ALPHA + x_ * (1 - ALPHA);
  float y_ = new_y * ALPHA + y_ * (1 - ALPHA);
  float rad = -atan2f(y_, x_);
  if (rad < 0)
  {
    rad += 2 * PI;
  }
  return rad * 57.2958;
  */
}

void IRAM_ATTR ISRPush()
{
  // MT6701 PUSH pin interupt
  push = true;
}

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println();
  Serial.println("wire setup");
  Wire.begin(SDAPin, SCLPin);
  Serial.println("wire setup ended");
  display.init();
  display.flipScreenVertically();
  display.setFont(ArialMT_Plain_10);
  display.drawString(2, 0, "MT6701 Sensor:");
  display.drawHorizontalLine(0, 10, 127);
  display.display();
  Serial.println("Display online");
  // interupts:  LOW: never seems to fire; HIGH: Pulses; RISING: pulses; FALLING: pusles 
  //attachInterrupt(PUSHPin, ISRPush, RISING);
  attachInterrupt(PUSHPin, ISRPush, FALLING);
  // attachInterrupt(PUSHPin, ISRPush, HIGH);
}

void loop()
{
  // put your main code here, to run repeatedly:

  float angle = MT6701_Read();
  Serial.println(angle);
  display.clear();
  display.drawString(2, 0, "MT6701 Sensor:");
  display.drawHorizontalLine(0, 10, 127);
  display.drawString(0, 24, "Angle:");
  display.drawString(38, 24, String(angle, 1));
  if (push == true)
  {
    // push button has been pressed
    display.drawString(0, 36, "PUSH");
    push = false;
  }
  display.display();
  delay(300);
}