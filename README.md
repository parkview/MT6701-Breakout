# MT6701_360_Degree_Magnetic_Encoder Breakout
  
A simple breakout PCB for the MT6701 magnetic field rotary encoder IC. There are two different IC footprints, one on either side.  There is provision for a 4-pin Picoblade (1.25mm pitch) or a 1.27mm pinheader connector for power and i2C data.
  
A bit more about the project: [https://forum.swmakers.org/viewtopic.php?f=9&t=2496](https://forum.swmakers.org/viewtopic.php?f=9&t=2496)  
  
  
## Hardware Features:
---------
#### Version 0.5 Features: 

* provision for either of the two MT6701 footprints
* 5-pin 1.5mm Picoblade connector for 3.3V power, i2C data and Push interrupt
* alternative 5-pin 1.27mm pin-header connector
* i2C Address 0x0B
* dual M1.0 mounting bolt/screw slots 
  
  
## Available Software 

  
#### Arduino:
* `MT6701-Mag-Encoder` - PlatformIO Arduino ESP32 example code that displays encoder readings on a SSD1306 OLED display 
